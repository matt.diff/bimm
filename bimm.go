package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math"
	"time"
)

const (
	initialTickrate     uint    = 60
	initialMoneyPerTick float64 = 0.005
	initialMoney        float64 = 0
	initialCostU1       float64 = 5
	initialCostU2       float64 = 500
)

type upgrade struct {
	id          uint
	level       uint
	initialCost float64
	nextCost    func(uint) float64
	multiplier  float64
}

func nextCostU1(level uint) float64 {
	if level == 0 {
		return initialCostU1
	}

	return math.Floor(math.Pow(float64((level+1)*5), 1.05))
}

func nextCostU2(level uint) float64 {
	if level == 0 {
		return initialCostU2
	}

	return math.Floor(math.Pow(float64((level+1)*500), 1.05))
}

type game struct {
	tickrate uint
	ticks    uint64
	money    float64
	buyQueue chan uint
	upgrades map[uint]upgrade
}

func NewGame() *game {
	return &game{
		tickrate: initialTickrate,
		ticks:    0,
		money:    initialMoney,
		buyQueue: make(chan uint, 60),
		upgrades: make(map[uint]upgrade),
	}
}

func (g *game) buyHandler() {
	var bid uint
	for {
		bid = <-g.buyQueue
		g.buyUpgrade(bid)
	}
}

func (g *game) buyUpgrade(bid uint) {
	upgrade := g.upgrades[bid-1]
	level := upgrade.level
	if g.money >= upgrade.nextCost(level) {
		g.money = g.money - upgrade.nextCost(level)
		upgrade.level++
		g.upgrades[bid-1] = upgrade
	}
}

func (g *game) initUpgrades() {
	u1 := upgrade{
		id:          1,
		level:       0,
		initialCost: 5,
		nextCost:    nextCostU1,
		multiplier:  0.01,
	}

	u2 := upgrade{
		id:          2,
		level:       0,
		initialCost: 500,
		nextCost:    nextCostU2,
		multiplier:  1.00,
	}

	g.upgrades[0] = u1
	g.upgrades[1] = u2
}

func (g *game) tick() {
	for {
		tickStart := time.Now().UnixNano()
		g.money = g.money + initialMoneyPerTick + (float64(g.upgrades[0].level) * g.upgrades[0].multiplier)
		g.ticks++
		if g.ticks%60 == 0 {
			g.metrics()
		}
		time.Sleep(16666667*time.Nanosecond - time.Duration(time.Now().UnixNano()-tickStart))
	}
}

type metrics struct {
	State metricsstate `json:"state"`
}

type metricsstate struct {
	Ticks    uint64            `json:"ticks"`
	Money    float64           `json:"money"`
	Upgrades []metricsupgrades `json:"upgrades"`
}

type metricsupgrades struct {
	Upgrade  string  `json:"upgrade"`
	Level    uint    `json:"level"`
	Nextcost float64 `json:"nextcost"`
}

func (g *game) metrics() {
	u1 := metricsupgrades{
		Upgrade:  "u1",
		Level:    g.upgrades[0].level,
		Nextcost: g.upgrades[0].nextCost(g.upgrades[0].level),
	}

	u2 := metricsupgrades{
		Upgrade:  "u2",
		Level:    g.upgrades[1].level,
		Nextcost: g.upgrades[1].nextCost(g.upgrades[1].level),
	}

	statepayload := metricsstate{
		Ticks:    g.ticks,
		Money:    (math.Round(g.money*1000) / 1000), //3 decimal places
		Upgrades: make([]metricsupgrades, 0, 2),
	}

	statepayload.Upgrades = append(statepayload.Upgrades, u1)
	statepayload.Upgrades = append(statepayload.Upgrades, u2)

	payload := metrics{
		State: statepayload,
	}
	jsonPayload, err := json.Marshal(payload)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(string(jsonPayload))
}

func testuser(g *game) {
	for {
		g.buyQueue <- 1
		time.Sleep(300 * time.Millisecond)
		g.buyQueue <- 2
		time.Sleep(800 * time.Millisecond)
	}
}

func main() {

	g := NewGame()
	g.initUpgrades()

	go g.buyHandler()
	go g.tick()

	time.Sleep(2 * time.Second)
	testuser(g)
}
